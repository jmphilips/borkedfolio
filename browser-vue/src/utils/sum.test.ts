import sum from './sum';
import { test, expect } from 'vitest';

test('sum', function () {
  const inputA = 1;
  const inputB = 2;
  const expected = 3;
  const output = sum(inputA, inputB);
  expect(output).toEqual(expected);
});
