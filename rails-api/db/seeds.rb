# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
#


bitcoin = Crypto.create({name: 'Bitcoin', symbol: 'BTC'})
ethereum = Crypto.create({name: 'Ethereum', symbol: 'ETH'})
cardano = Crypto.create({name: 'Cardano', symbol: 'ADA'})

user = User.first.nil? ? User.create({email: 'jmichaelphilips@gmail.com'}) : User.first
account = Account.first.nil? ? Account.create({institution: 'ledger', asset_type: 'crypto', user: user}) : Account.first
bitcoin_holding = Holding.create({account: account, amount: 0.471371, crypto: bitcoin })
ethereum_holding = Holding.create({account: account, amount: 1.11612, crypto: ethereum })