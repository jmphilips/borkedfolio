class AddAssetToHoldings < ActiveRecord::Migration[7.0]
  def change
    add_reference :assets, :holdings
  end
end
