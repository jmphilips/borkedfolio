class AddCryptoToHoldings < ActiveRecord::Migration[7.0]
  def change
    add_reference :holdings, :crypto, foreign_key: true
  end
end
