class RemoveAssetsTable < ActiveRecord::Migration[7.0]
  def up
    drop_table :assets
  end

  def down
    create_table :assets do |t|
      t.bigint :crypto_id, null: false
      t.datetime :created_at, null: false
      t.datetime :updated_at, null: false
    end

    add_foreign_key :assets, :crypto
  end
end
