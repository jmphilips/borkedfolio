class Crypto < ApplicationRecord
  validates :name, presence: true
  validates :symbol, presence: true, uniqueness: true
  def fetch_price
    CryptoService.fetch_crypto_price(name.downcase)
  end
end
