class AccountsController < ApplicationController
  def create
    account = Account.create(create_params)
    render json: { account: account }
  end

  def index
    accounts = params.nil? ? Account.all : Account.where(user_id: params[:user_id])
    accounts = Account.all
    render json: { accounts: accounts }
  end

  def show
    account = Account.find(params[:id])
    if account
      render json: { account: account }
    else
      render json: { message: "Not Found" }, status: :not_found
    end
  end

  def update
  end

  private

  def create_params
    params.require(:account).permit(:asset_type, :user_id, :institution)
  end
end
