class UsersController < ApplicationController
  def index
    if params[:email]
      user = User.find_by(email: params[:email])
      render json: { user: user }
    else
      users = User.all
      render json: { users: users }
    end
  end

  def create
    user = User.create(create_params)
    render json: { user: user }
  end

  def show
    user = User.find(params[:id])
    render json: { user: user }
  end

  private

  def create_params
    params.require(:user).permit(:email)
  end

  def show_params
    params.require(:user).permit(:email)
  end
end