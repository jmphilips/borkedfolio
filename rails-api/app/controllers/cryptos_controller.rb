class CryptosController < ApplicationController
  def index
    currencies = Crypto.all
    render json: { data: currencies }
  end

  def show
    crypto = Crypto.find(params[:id])
    price = CryptoService.fetch_crypto_price(crypto.name)

    render json: { crypto: crypto, price: price}
  end

  def  price
    crypto = Crypto.find(params[:id])
    price = CryptoService.fetch_crypto_price(crypto.name)
    render json: { price: price }
  end
end
