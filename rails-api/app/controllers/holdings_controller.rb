class HoldingsController < ApplicationController
  def index
    if params[:account_id]
      holdings = Holding.where(account_id: params[:account_id])
      render json: { holdings: holdings }
    else
      holdings = Holding.all
      render json: { holdings: holdings }
    end
  end

  def create

  end

  def create_params
    params.require(:holding).permit(:account_id, :amount, :asset_id)
  end
end
