class CryptoService
  def self.fetch_crypto_price(asset_string)
    url = build_asset_url(asset_string.downcase)
    conn = http_get(url)
    parse_http_request(conn)
  end

  private

  def self.http_get(url)
    Faraday.get(url)
  end

  def self.build_asset_url(asset_name)
    coin_cap_url + asset_name
  end

  def self.parse_http_request(conn)
    response = conn.to_hash
    puts response
    json_response = JSON.parse(response[:body])
    json_response["data"]
  end

  def self.coin_cap_url
    "https://api.coincap.io/v2/assets/"
  end
end
