Rails.application.routes.draw do
  post 'accounts', to: 'accounts#create'
  get 'accounts', to: 'accounts#index'
  get 'accounts/:id', to: 'accounts#show'
  put 'accounts/:id', to: 'accounts#update'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  get '/cryptos', to: 'cryptos#index'
  get '/cryptos/:id', to: 'cryptos#show'
  get '/cryptos/:id/price', to: 'cryptos#price'

  post '/holdings', to: 'holdings#create'
  get '/holdings', to: 'holdings#index'

  get '/users', to: 'users#index'
  post '/users', to: 'users#create'
  get '/users/:id', to: 'users#show'
end
