require "test_helper"

class CryptoControllerTest < ActionDispatch::IntegrationTest
  test "should get index," do
    get crypto_index,_url
    assert_response :success
  end

  test "should get create," do
    get crypto_create,_url
    assert_response :success
  end

  test "should get update," do
    get crypto_update,_url
    assert_response :success
  end

  test "should get get" do
    get crypto_get_url
    assert_response :success
  end
end
