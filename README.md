# Borkedfolio

## This is a repo of fun stuff made by Joshua Philips.

## Running the project

To run the proeject using docker, ensure that docker is installed and then use the following command:

`docker compose up -d --build`

This will build the containers and start the following running in the background:

* postgresql
* rails-api
* browser-vue


## Folders

### browser-vue

[README](./browser-vue/README.md)

### rails-api

[README](./rails-api/README.md)

### docs

Documentation Repo

* [gitlab-ci](./docs/gitlab-ci.md)
* [conventions and standards](./docs/convetions-and-standards.md)