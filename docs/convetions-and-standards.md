# Conventions and Standards

## Git

### Branch Naming

This repository is a monorepo of different tools, languages and technologies, and therefore standard naming conventions of things like branches should help to keep the commit history neat, clean, and easy to reason about.

The directory structure is as follows:

* borkfolio
  * rails-api
  * browser-vue
  * docs

The structure of git branches should be the project or app followed by a forward slash followed by the feature or fix work that is being implemented.  For example, if I was to add a new title component to the `browser-vue` application, then I would name the branch `browser-vue/title-component`.

`browser-vue`: name of app or project.
`/`: Separates project and app from feature or fix.
`title-component`: Name of feature or fix that is being worked on or implemented.

For project wide work such as Gitlab or Docker that spans across multiple sub directories, `project` can be used.

